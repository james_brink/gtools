package main

import (
	"log"
	"os"

	"bitbucket.org/james_brink/gtools/commands"

	"github.com/urfave/cli"
)

func main() {

	app := cli.NewApp()
	app.Name = "Gource Tool"
	commands.SubCommand(app)
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
