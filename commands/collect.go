package commands

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strconv"

	"github.com/urfave/cli"
)

type fileChange struct {
	Operation string
	Path      string
}

type commit struct {
	User  string
	Date  int64
	Files []fileChange
}

type commits []commit

func (c commits) Len() int {
	return len(c)
}

func (c commits) Less(i, j int) bool {
	return c[i].Date < c[j].Date
}

func (c commits) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

// SubCommand initilizes collect on App
func SubCommand(app *cli.App) {
	var directory string
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	var cmd cli.Command
	cmd.Name = "collect"
	cmd.Aliases = []string{"c"}
	cmd.Usage = "o generate log files to be consumed by Gource."
	cmd.Action = execute
	var stringFlag cli.StringFlag
	stringFlag.Name = "directory, d"
	stringFlag.Value = cwd
	stringFlag.Usage = "Directory `path` containing a git repository."
	stringFlag.Destination = &directory
	cmd.Flags = append(cmd.Flags, stringFlag)
	app.Commands = append(app.Commands, cmd)
}

func printCommits(commits []commit) {
	for _, reflogCommit := range commits {
		for _, file := range reflogCommit.Files {
			fmt.Printf("%d|%s|%s|/%s\n", reflogCommit.Date, reflogCommit.User,
				file.Operation, file.Path)
		}
	}
}

func execute(c *cli.Context) error {
	directory := c.String("directory")
	reflog := execGit(directory)
	reflogCommits := parseGitLog(reflog)
	sort.Sort(reflogCommits)
	printCommits(reflogCommits)
	return nil
}

func parseGitLog(l string) commits {
	re, err := regexp.Compile(`(?m)^$\n`)
	res := re.Split(l, -1)
	userDateRe, err := regexp.Compile(`(?m)^user:(.+)$\n^(\d+)$\n`)
	fileRe, err := regexp.Compile(`(?m)(^:.+\s+)(A|M|D)\s+(.+)$`)
	if err != nil {
		log.Fatal(err)
	}
	var reflogCommits commits
	for _, c := range res {
		userMatch := userDateRe.FindStringSubmatch(c)
		fileMatch := fileRe.FindAllStringSubmatch(c, -1)
		if len(userMatch) > 0 && len(fileMatch) > 0 {
			user := userMatch[1]
			date, err := strconv.ParseInt(userMatch[2], 10, 64)
			if err != nil {
				log.Fatal(err)
			}
			var reflogFileChanges []fileChange
			for _, f := range fileMatch {
				reflogFileChange := fileChange{f[2], f[3]}
				reflogFileChanges = append(reflogFileChanges, reflogFileChange)
			}
			reflogCommit := commit{user, date, reflogFileChanges}
			reflogCommits = append(reflogCommits, reflogCommit)
		}
	}
	return reflogCommits
}

func execGit(d string) string {
	gitLogFmt := "--pretty=format:user:%aN%n%ct"
	cmd := exec.Command("/usr/bin/env", "git", "--no-pager", "--git-dir", d+"/.git",
		"log", gitLogFmt, "--reverse", "--raw", "--encoding=UTF-8", "--no-renames")
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Printf("%s\n", stdoutStderr)
		log.Fatal(err)
	}
	return string(stdoutStderr[:])
}
